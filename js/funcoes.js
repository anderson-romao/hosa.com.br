
// Fechar menu mobile
$(function(){ 
     var navMain = $(".navbar-collapse");
     navMain.on("click", "a:not([data-toggle])", null, function () {
         navMain.collapse('hide');
     });
 });

// Home -> Carousel
$(document).ready(function(){
	$('.new-customer-carousel').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 10000,
		arrows: false,
		dots: false,
		fade: true,
		prevArrow:'<img src="imagens/logos/left-arrow.png" class="btn-slick-carousel-left" width="30" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/right-arrow.png" class="btn-slick-carousel-right" width="30" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
	});
});

// Home -> Exames
$(document).ready(function(){
	$('.customer-exames').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: true,
		dots: true,
		prevArrow:'<img src="imagens/logos/left-arrow.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/right-arrow.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: false,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
				arrows: false,
				centerMode: true,
				variableWidth: true,
			}
		}
		]
	});
});

// Home -> Corpo medico
// Centro cirurgico -> Corpo medico
$(document).ready(function(){
	$('.customer-corpo-medico').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 7000,
		arrows: true,
		dots: true,
		prevArrow:'<img src="imagens/logos/icon-arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/icon-arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: false,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 2,
				arrows: false,
				centerMode: true,
				variableWidth: true,
			}
		}
		]
	});
});

// Home -> Estrutura
$(document).ready(function(){
	$('.customer-estrutura').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: false,
		dots: true,
		pauseOnHover: false,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
				centerMode: true,
				variableWidth: true,
			}
		}
		]
	});
});

// Home -> Convenios
$(document).ready(function(){
	$('.customer-convenios').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: false,
		dots: true,
		prevArrow:'<img src="imagens/logos/left-arrow.png" class="btn-slick-carousel-left" width="30" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/right-arrow.png" class="btn-slick-carousel-right" width="30" alt="Próximo" title="Próximo">',
		pauseOnHover: false,
	});
});

// Home -> Noticias
$(document).ready(function(){
	$('.customer-noticias').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: false,
		dots: true,
		pauseOnHover: false,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3
				}
			}, {
				breakpoint: 769,
				settings: {
					slidesToShow: 3
				}
			}, {
				breakpoint: 520,
				settings: {
					slidesToShow: 1,
					centerMode: true,
					variableWidth: true,
				}
			}
		]
	});
});



// Hosa -> Nossa Tragetoria
$(document).ready(function() {
	$('.customer-tragetoria').slick({
		slidesToShow: 1,
		centerMode: true,
		centerPadding: '0px',
		autoplay: true,
		autoplaySpeed: 5000,
		variableWidth: true,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/icon-arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/icon-arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
	});
});

// Hosa -> Nossa equipe
$(document).ready(function(){
	$('.customer-nossa-equipe').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 7000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/icon-arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/icon-arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: false,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 2,
				arrows: false,
				dots: true,
				centerMode: true,
				variableWidth: true,
			}
		}
		]
	});
});


// Exames -> tipos de exames
$(document).ready(function(){
	$('.customer-tipos-exames').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 7000,
		arrows: false,
		dots: true,
		pauseOnHover: false,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 1
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 1
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});
});


// Centro Cirúrgico -> Boxes
$(document).ready(function(){
	$('.customer-boxes').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 7000,
		arrows: false,
		dots: true,
		pauseOnHover: false,
		variableWidth: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 4
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 4
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 2,
				dots: true,
			}
		}
		]
	});
});


//Iniciar animações
AOS.init({
	// disable: 'mobile',
	delay: 0,
	duration: 1200,
	once: true,
});